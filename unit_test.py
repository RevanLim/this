import unittest
import datetime
from datetime import timedelta
import sys
from unittest.mock import patch, MagicMock
from t import tweetAnalyzer

class tweetAnalyzerTests(unittest.TestCase):
    def setUp(self):
        """
        Setting up an instance of tweetAnalyser to test it
        :return:
        """
        self.twit = tweetAnalyzer()

    def tearDown(self):
        """
        Restoring login credentials in the case of test methods changing them
        """
        #user_token
        self.twit.chgCredentials(0, "w1UrNY5hjcH3m0bRzIORCXCYA")
        #user_secret
        self.twit.chgCredentials(1, "hdSSBrq8jMQvLx01190hDpyOrg1Ns3CYJ7KbSCSrvTqFniWxji")
        #app_token
        self.twit.chgCredentials(2, "414814656-MfXapCu3ruf2h8tuTkJZ4yp4FD0fb5rRZyJ7AZRt")
        #app_secret
        self.twit.chgCredentials(3, "lFEeuG9U65q8p2cHxTdkI6vGYKoE2iN7Ems1MniB5Wasf")

    #findTweetsAfter
    def test_valid_dateafter(self):
        """
        Testing the findTweetsAfter method using the date 2016/10/10 to see if it
        modifies the variable after_date
        :return:
        """
        self.assertEqual(self.twit.after_date, datetime.datetime(1,1,1))
        testdate = datetime.datetime(2016,10,10)
        self.twit.findTweetsAfter("2016-10-10")
        self.assertEqual(self.twit.after_date, testdate)

    def test_invalid_dateafter(self):
        """
        Testing the findTweetsAfter method using the date 2018/10/10 which is in the
        future and hence should raise an error
        :return:
        """
        with self.assertRaises(AssertionError):
            self.twit.findTweetsAfter("2018-10-10")

        self.twit.before_date = datetime.datetime(2017, 1, 1)
        with self.assertRaises(AssertionError):
            self.twit.findTweetsAfter("2017-03-04")

    #findTweetsBefore
    def test_valid_datebefore(self):
        """
        Testing the findTweetsBefore method using the date 2016/10/10 to see if it
        modifies the variable before_date
        :return:
        """
        self.assertTrue(self.twit.before_date >= datetime.datetime.today() - timedelta(minutes=1))
        testdate = datetime.datetime(2016, 10, 10)
        self.twit.findTweetsBefore("2016-10-10")
        self.assertEqual(self.twit.before_date, testdate)

    def test_invalid_datebefore(self):
        """
        Testing the findTweetsBefore method using the date 2018/10/10 which is in the
        future and hence should raise an error
        :return:
        """
        with self.assertRaises(AssertionError):
            self.twit.findTweetsBefore("2018-10-10")

        self.twit.after_date = datetime.datetime(2017, 5, 1)
        with self.assertRaises(AssertionError):
            self.twit.findTweetsBefore("2017-04-01")

    #specifyID
    def test_valid_ID(self):
        """
        Testing the specifyID method using the string @Iandawarrior to check if it modifies
        the variable twitter_handle
        :return:
        """
        self.twit.specifyID("@Iandawarrior")
        testid = self.twit.twitter_handle
        self.assertEqual("@Iandawarrior", testid)

    def test_invalid_ID(self):
        """
        Testing the specifyID method using the string Challenger to check if it raises an
        exception because the first letter is not the @ symbol
        :return:
        """
        with self.assertRaises(AssertionError):
            self.twit.specifyID("Challenger")

    #listTopWords
    def test_valid_T(self):
        """
        Testing the listTopWords method using the integer 43 to check if it modifies the
        variable list_limit
        :return:
        """
        self.twit.listTopWords(43)
        self.assertEqual(self.twit.list_limit, 43)

    def test_invalid_T(self):
        """
        Testing the listTopWords method using the character a to check if it raises an
        exception because the input should be an integer
        :return:
        """
        with self.assertRaises(AssertionError):
            self.twit.listTopWords("a")
            self.twit.listTopWords(-1)

    #minCount
    def test_valid_minCount(self):
        """
        Testing the minCount method using the integer 43 to check if it modifies the
        variable min_count
        :return:
        """
        self.twit.minCount(43)
        self.assertEqual(self.twit.min_count, 43)

    def test_invalid_minCount(self):
        """
        Testing the minCount method using the character a to check if it raises an
        exception because the input should be an integer
        :return:
        """
        with self.assertRaises(AssertionError):
            self.twit.minCount("a")
            self.twit.minCount(-1)

    #maxTweets
    def test_valid_maxTweets(self):
        """
        Testing the maxTweets method using the integer 43 to check if it modifies the
        variable max_results
        :return:
        """
        self.twit.maxTweets(43)
        self.assertEqual(self.twit.max_results, 43)

    def test_invalid_maxTweets(self):
        """
        Testing the maxTweets method using the integer 501 to check if it raises an
        exception because the maximum tweets allowed is 500
        :return:
        """
        with self.assertRaises(AssertionError):
            self.twit.maxTweets(501)
            self.twit.maxTweets(-1)
            self.twit.maxTweets("this")

    #excludeString
    def test_valid_excludeString(self):
        """
        Testing the excludeString method using the list with strings jungler, never, gank
        to check if it modifies the variable excluded_strings
        :return:
        """
        testlist = ["jungler", "never", "gank"]
        self.twit.exclude_strings(testlist)
        self.assertTrue("jungler" in self.twit.excluded_strings)
        self.assertTrue("never" in self.twit.excluded_strings)
        self.assertTrue("gank" in self.twit.excluded_strings)
        self.assertFalse("just" in self.twit.excluded_strings)
        self.assertFalse("farm" in self.twit.excluded_strings)

    def test_invalid_excludeString(self):
        """
        Testing the excludingString method using the integer 510 to check if it raises
        and exception because input has to be a list of strings
        :return:
        """
        with self.assertRaises(AssertionError):
            self.twit.exclude_strings(510)
            self.twit.exclude_strings(["hi"])

    #chgCredentials
    def test_valid_chgCredentials(self):
        """
        Testing the chgCredentials method using different strings for every internal variable
        and seeing if it altered individual variable
        :return:
        """
        #user_token
        self.twit.chgCredentials(0, "somerandomstring0")
        self.assertEqual(self.twit.user_token, "somerandomstring0")

        #user_secret
        self.twit.chgCredentials(1, "somerandomstring1")
        self.assertEqual(self.twit.user_secret, "somerandomstring1")

        #app_token
        self.twit.chgCredentials(2, "somerandomstring2")
        self.assertEqual(self.twit.app_token, "somerandomstring2")

        #app_secret
        self.twit.chgCredentials(3, "somerandomstring3")
        self.assertEqual(self.twit.app_secret, "somerandomstring3")

    def test_invalid_chgCredentials(self):
        """
        Testing the chgCredentials method using the integer 7938 for each possible internal
        variable to check if it raises an exception because input has to be a string
        :return:
        """
        with self.assertRaises(AssertionError):
            self.twit.chgCredentials(0, 7938)
            self.twit.chgCredentials(1, 7938)
            self.twit.chgCredentials(2, 7938)
            self.twit.chgCredentials(3, 7938)

    #sortWords
    def test_valid_sortWords(self):
        """
        Testing the sortWords method using the string lala to check if the internal
        dictionary stores the word in the right place
        :return:
        """
        testlist = self.twit.sortWords("la la ba j(o)p")
        self.assertEqual(testlist[0][1], "la")
        self.assertEqual(testlist[0][0], 2)
        self.assertFalse((1, "j(o)p") in testlist)

    def test_invalid_sortWords(self):
        """
        Testing the sortWords method using the integer 7938 to check if it raises an
        exception because expected input is a string
        :return:
        """
        with self.assertRaises(AssertionError):
            self.twit.sortWords(7938)
            self.twit.sortWords(["a", "long", "list"])

    #parseArgs
    def test_valid_parseArgs(self):
        """
        Testing the parseArgs method by creating an argv object then parsing the input
        values with all the inputs as below and checking if the associated variable is
        modified accordingly
        :return:
        """
        sys.argv = ['', "-s", "this", "that", "-m", "30", "-o", "hvsc", "-p", "dfj", "-u", "sdfj",
                    "-x", "secret?", "-a", "2016-11-12", "-i", "@gitlabError", "-c", "320"]
        args = self.twit.parseArgs()
        self.assertEqual(args.s, ["this", "that"])
        self.assertEqual(args.m, 30)
        self.assertEqual(args.o, "hvsc")
        self.assertEqual(args.p, "dfj")
        self.assertEqual(args.u, "sdfj")
        self.assertEqual(args.x, "secret?")
        self.assertEqual(args.a, "2016-11-12")
        self.assertEqual(args.b, None)
        self.assertEqual(args.i, "@gitlabError")
        self.assertEqual(args.t, None)
        self.assertEqual(args.c, 320)

    #pass args from cmd to instance variables
    def test_valid_passArgs(self):
        """Testing the passArgs method by passing all the arguments from the parseArgs
        method into the passArgs method and checking if associated variable was modified
        accordingly
        """
        sys.argv = ['', "-s", "this", "that!", "-m", "300", "-o", "hvsc", "-p", "dfj", "-u", "sdfj",
                    "-x", "secret?", "-a", "2016-11-12", "-b", "2017-09-10", 
                    "-i", "@gitlabError", "-t", "25", "-c", "10"]
        self.twit.passArgs(self.twit.parseArgs())
        self.assertTrue("this" in self.twit.excluded_strings)
        self.assertFalse("that!" in self.twit.excluded_strings)
        self.assertEqual(self.twit.max_results, 300)
        self.assertEqual(self.twit.app_token, "hvsc")
        self.assertEqual(self.twit.app_secret, "dfj")
        self.assertEqual(self.twit.user_token, "sdfj")
        self.assertEqual(self.twit.user_secret, "secret?")
        self.assertEqual(self.twit.after_date, datetime.datetime(2016, 11, 12))
        self.assertEqual(self.twit.before_date, datetime.datetime(2017, 9, 10))
        self.assertEqual(self.twit.twitter_handle, "@gitlabError")
        self.assertEqual(self.twit.list_limit, 25)
        self.assertEqual(self.twit.min_count, 10)

    #analyze tweets
    @patch("t.tweetAnalyzer.getTweets")
    def test_getTweets(self, mock):
        """
        Testing the function to get tweets to be analyzed. The api components are mocked out, and a
        preset list of tuples containing words and their frequency is fed instead to test the
        functionality of the method.
        """
        mock.return_value = "one long list of strings with some repeated repeated strings with one new: new: new: new:"
        sys.argv = ['']
        self.twit.passArgs(self.twit.parseArgs())
        wordstring = self.twit.getTweets()

        self.assertTrue(mock.call_count == 1)

if __name__ == '__main__':
    unittest.main()
