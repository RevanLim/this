"""
Team: Chocolate Cheesecake
FIT2107 Assignment 3

Options:
-s SEARCH_STRING : a search string using the Twitter search format.
-m MAX_RESULTS : The maximum number of tweets to analyse, as returned
by the Twitter search API. The default should be 100, and the maximum
should be 500. If more than 500 are selected, the program should abort
with an error message.
-o APPLICATION_ACCESS_TOKEN : A Twitter application access token
(required to access the Twitter API)
-p APPLICATION_ACCESS_TOKEN_SECRET : The application access token secret
(required to access the Twitter API).
-u USER_ACCESS_TOKEN : The user's application access token
-x USER_ACCESS_TOKEN_SECRET : The user's access token secret.
-a DATE : include results only from DATE. Specified as YYYY-MM-DD. If
omitted, include results from the beginning of Twitter's available history.
If after the present date, abort with an error message.
-b DATE : include results only until DATE. Specified as YYYY-MM-DD. If
omitted, include results up to the present. If after the present date, or
equal to or before the argument for -a, abort with an error message.
-i ID : An id of a specific twitter user to analyse. ID should start with
the @ sign. If omitted, the search should include all users.
-t T : list at most the top T words in the set. If omitted, the default is 10.
- c MIN_COUNT : only list words that occur MIN_COUNT or more times.
"""
import datetime
import string
import operator
import argparse

class tweetAnalyzer():
    def __init__(self):
        self.user_token, self.user_secret, self.app_token, self.app_secret = self.readlogin()
        self.excluded_strings = []
        self.max_results = 100
        self.after_date = datetime.datetime.min
        self.before_date = datetime.datetime.today()
        self.twitter_handle = None
        self.list_limit = 10
        self.min_count = 0

    def readlogin(self):
        """
        Method reads login.txt to retrieve the login details of the user in order to use
        the tweepy API.
        """
        file = open("login.txt")
        out = []
        for line in file:
            out.append(line.strip('\n'))
        file.close()
        return out

    def sortWords(self, words):
        """
        Method sorts all words in a string based on the frequency of its occurance.
        Words in the exclude list will not be included in the results.

        Returns a list of tuples, [(freq1, word1), (freq2, word2),...] with words and their
        corrosponding frequency, sorted in descending order.
        Length of list corrosponds to self.list_limit, if defined by the user, else defaults
        to 10. Actual length of the list may be longer.
        """
        assert isinstance(words, str)
        d = {}
        bad_char = string.punctuation + "\"\'"
        wordlist = words.split("\n")
        for line in wordlist:
            line = line.split(" ")
            for word in line:
                if any(char in bad_char for char in word) or word == '':
                    continue
                word = word.lower().strip("\n")
                if word in d and word not in self.excluded_strings:
                    d[word] += 1
                else:
                    d[word] = 1

        sorted_d = sorted(d.items(), key=operator.itemgetter(1), reverse = True)

        i = 0
        analysis = []
        for word in sorted_d:
            if word[1] >= self.min_count:
                analysis.append((word[1], word[0]))
                i += 1
                if i >= self.list_limit:
                    break
        return analysis

    def parseArgs(self):
        parser = argparse.ArgumentParser(description='analyze tweets')
        parser.add_argument("-s", type = str, nargs = "+", help = "Enter string to be excluded from search")
        parser.add_argument("-m", type = int, help = "Enter maximum number of tweets to analyze")
        parser.add_argument("-o", type = str, help = "Enter application access token")
        parser.add_argument("-p", type = str, help = "Enter application access token secret")
        parser.add_argument("-u", type = str, help = "Enter user access token")
        parser.add_argument("-x", type = str, help = "Enter user access token secret")
        parser.add_argument("-a", type = str, help = "Enter DATE (yyyy-mm-dd) to only find results after DATE or as far back as possible otherwise")
        parser.add_argument("-b", type = str, help = "Enter DATE (yyyy-mm-dd) to only find results before DATE or up to the present")
        parser.add_argument("-i", type = str, help = "Enter twitter handle of user to analyze, if not analyze all users")
        parser.add_argument("-t", type = int, help = "Enter number of words to show")
        parser.add_argument("-c", type = int, help = "Enter number such that only words with a greater number of occurances than this number will be shown")
        args = parser.parse_args()
        
        return args

    def exclude_strings(self, bad_str):
        """
        Method keeps a list of strings to be excluded from the result list.
        """
        assert isinstance(bad_str, list)
        bad_char = string.punctuation + "\"\' "
        for word in bad_str:
            word = word.lower().strip("\n")
            if not any(c in bad_char for c in word):
                self.excluded_strings.append(word)
        return

    def chgCredentials(self, option, credential):
        """
        Changes the login credentials permanently, following the option given.
        Options:
            0 - Application access token
            1 - Application access token secret
            2 - Application user token
            3 - Application user token secret

        Note: All four credentials must be correct for the program to access the tweepy
        module, this function does not verify its accuracy. If invalid login details are
        entered, it is not possible to revert changes, users must find the correct login
        details and add it back manually.
        """
        assert option >= 0 and option <= 3, "out of valid range"
        assert isinstance(credential, str)
        loginfile = open("login.txt")
        info = []
        for line in loginfile:
            info.append(line.strip("\n"))
        loginfile.close()

        info[option] = credential
        newloginfile = open("login.txt", "w")
        s = "\n"
        s = s.join(info)
        newloginfile.write(s)
        newloginfile.close()

        if option == 0:
            self.user_token = credential
        elif option == 1:
            self.user_secret = credential
        elif option == 2:
            self.app_token = credential
        else:
            self.app_secret = credential

        return

    def findTweetsAfter(self, datestring = None):
        """
        Method sets self.after_date to a date if given, or to the epoch otherwise.
        Only tweets after this date will be analyzed.

        Datestring must be in the format YYYY-MM-DD.
        self.after_date must be a date before the present day, and after self.before_date.
        """
        if datestring is not None:
            date = datestring.split("-")
            assert len(date) == 3, "Invalid date format"
            self.after_date = datetime.datetime(int(date[0]), int(date[1]), int(date[2]))

        assert self.after_date <= self.before_date, "Invalid range for dates set"
        assert self.after_date <= datetime.datetime.today(), "Date given cannot be in the future"
        return

    def findTweetsBefore(self, datestring = None):
        """
        Method sets self.before_date to a date if given, or to the present day otherwise.
        Only tweets posted before this date will be analyzed.

        Datestring must be in the format YYYY-MM-DD.
        self.before_date must be a date before the present day, and before self.after_date.
        """
        if datestring is not None:
            date = datestring.split("-")
            assert len(date) == 3, "Invalid date format"
            self.before_date = datetime.datetime(int(date[0]), int(date[1]), int(date[2]))

        assert self.after_date <= self.before_date, "Invalid range for dates set"
        assert self.before_date <= datetime.datetime.today(), "Date given cannot be in the future"
        return

    def specifyID(self, ID = None):
        """
        Method to search for a specific twitter user to analyse. ID should start with the @ sign. If omitted, the
        search should include all users
        :param ID:ID of user to analyse
        :return:
        """
        self.twitter_handle = ID
        if self.twitter_handle is not None:
            #search id only
            assert self.twitter_handle[0] == "@", "All twitter ID's must start with an @ symbol"
        return

    def maxTweets(self, max_num):
        """
        The maximum number of tweets to analyse, as returned by the Twitter
        search API. The default should be 100, and the maximum should be 500. If more than 500 are
        selected, the program should abort with an error message
        :param max_num:
        :return:
        """
        assert isinstance(max_num, int) and max_num > 0, "Maximum number of tweets must be a positive integer"
        assert max_num <= 500, "Too many tweets, limit is 500"
        self.max_results = max_num

    def listTopWords(self, T = 10):
        """
        Method to list at most the top T words in the set. If omitted, the default is 10.
        :param T:Number of words to list
        :return:
        """
        assert isinstance(T, int) and T > 0, "Number of words must be a positive integer"
        self.list_limit = T

    def minCount(self, minC = 0):
        """
        Method to list words that occur minC or more times.
        :param minC:default is 0
        :return:
        """
        assert isinstance(minC, int) and minC > 0, "Minimum count must be a positive integer"
        self.min_count = minC

    def passArgs(self, args):
        """
        Method passes the command line arguments into the instance variables using
        the other methods in this class where applicable.
        """
        if args.s:
            self.exclude_strings(args.s)
        if args.m:
            self.maxTweets(args.m)
        if args.o:
            self.chgCredentials(2, args.o)
        if args.p:
            self.chgCredentials(3, args.p)
        if args.u:
            self.chgCredentials(0, args.u)
        if args.x:
            self.chgCredentials(1, args.x)
        if args.a:
            self.findTweetsAfter(args.a)
        if args.b:
            self.findTweetsBefore(args.b)
        if args.i:
            self.specifyID(args.i)
        if args.t:
            self.listTopWords(args.t)
        if args.c:
            self.minCount(args.c)

    def getTweets(self):
        """
        Method calls the tweepy api with the given constraints and credentials, and retrieves a
        string of words containing tweets.

        Returns an unaltered string of space seperated words.
        """
        import tweepy
        #get tweets using all constraints specified earlier
        auth = tweepy.OAuthHandler(self.user_token, self.user_secret)
        auth.set_access_token(self.app_token, self.app_secret)
        api = tweepy.API(auth)

        if self.twitter_handle is None:
            tweets = tweepy.Cursor(api.home_timeline).items(self.max_results)
        else:
            tweets = tweepy.Cursor(api.user_timeline, id=self.twitter_handle).items(self.max_results)
        out = ""

        for tweet in tweets:
            if tweet.created_at <= self.before_date and tweet.created_at >= self.after_date:
                out += tweet.text.strip("\n")
                out += " "
        return out

if __name__ == "__main__":
    T = tweetAnalyzer()
    T.passArgs(T.parseArgs())
    rankedwords = T.sortWords(T.getTweets())
    for line in rankedwords:
        print(line[0], "\t", line[1])
